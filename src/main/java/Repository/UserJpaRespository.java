/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Repository;

import com.example.ProjectSample.demo.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

/**
 *
 * @author Personal
 */
@Component
public interface UserJpaRespository extends JpaRepository<Users, Long> {

    Users findByName(String name);
    
}
