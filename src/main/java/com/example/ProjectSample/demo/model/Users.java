package com.example.ProjectSample.demo.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

@Entity
public class Users {

    public Users(Long Id, String name, String TeamName, Integer salary) {
        this.Id = Id;
        this.name = name;
        this.TeamName = TeamName;
        this.salary = salary;
    }
	private Long Id;
	private String name;
	private String TeamName;
	private Integer salary;
	
	@Id
	@GeneratedValue
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTeamName() {
		return TeamName;
	}
	public void setTeamName(String teamName) {
		TeamName = teamName;
	}
	public Integer getSalary() {
		return salary;
	}
	public void setSalary(Integer salary) {
		this.salary = salary;
	}
}
