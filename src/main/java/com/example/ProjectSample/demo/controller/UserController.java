/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.ProjectSample.demo.controller;

import Repository.UserJpaRespository;
import com.example.ProjectSample.demo.model.Users;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Personal
 */

@RestController
@RequestMapping("/users")
public class UserController {
//    @GetMapping(value = "/cobain")
//    public String cobain()
//    {
//        return "nyoba";
//    }
    
    @Autowired
    private UserJpaRespository userJpaRespository;
    @GetMapping(value = "/all")
    public List<Users> findAll(){
        return userJpaRespository.findAll();
        
    }
    
    @GetMapping(value = "/{name}")
    public Users findByName(@PathVariable final String name){
        return userJpaRespository.findByName(name);
         
    }
    
    @PostMapping(value = "/load")
    public Users load(@RequestBody final Users users){
        userJpaRespository.save(users);
        return userJpaRespository.findByName(users.getName());
    }
    

    
    
}
